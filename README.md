# System bootstrap repository

This is a collection of resources for bootstrapping computer systems into
a state where they have a reasonable operating system installed

This used to be a part of the eyeo/devops/playground/utilities repository

## Bootstrap

This is the script we use to bootstrap physical servers into a state where
Ansible can be run on them.

Usage instructions:

    ./bootstrap.sh -h

### Prerequisites

The bootstrap script requires a working Debian-based rescue/live environment
from which the system-to-be-bootstrapped can be accessed.

On OVH and Hetzner, this is provided by them, their rescue images work like
this.

On other, lesser IaaS providers, this may have to be done manually, for
example by using the Debian Live CD image. Typically, one connects to the
server's KVM interface, attach such an image file as a virtual CD, and boot
into it, after which point you have to set up networking and then set up
SSH:

    sudo apt install openssh-server

If the KVM solution has a way to paste large amounts of text, you can use it
to edit /root/.ssh/authorized_keys. If not, set a temporary password for
root, enable root login via SSH with password, and immediately after the
first login, disable the latter.

    sudo -e /etc/ssh/sshd_config
    # PermitRootLogin yes
    # PasswordAuthentication on
    sudo passwd root
    sudo service ssh restart

From your machine, run `ssh root@<that IP>` and then

    mkdir .ssh
    tee .ssh/authorized_keys
    # paste the list of keys to allow
    # Ctrl+D
    sudo -e /etc/ssh/sshd_config
    # undo previous changes
    sudo service ssh restart

Then repeat `ssh root@<that IP>` once again to verify your key is working,
and at that point you can use the bootstrap script.
