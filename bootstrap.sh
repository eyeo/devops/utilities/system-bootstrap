#!/bin/sh -e
#
# Copyright (c) 2018-present eyeo GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Ensure the entire script to abort on error:
set -e

authorized_keys=/root/.ssh/authorized_keys
basename="`basename \"$0\"`"
min_bootstrap_phase=10
want_single_disk_device=
hostname=
console_options="console=tty0"
network_device_options="net.ifnames=0 biosdevname=0"
early_ip_options=
mirror="http://deb.debian.org/debian/"
distribution="debian"
release="buster"
release_version=
uuid=
root_disk_size="20G"
extra_efi_rules=
efi_optional="true"
bios_boot_partition_type="ef02"
ipv4_network_point_to_point=
ipv6_network_point_to_point=
partition_delimiter=
network_bonding_bridge=1
auto_nic_grub=
extra_release_key=
identities_file=

map_release_version() {
    case "$release" in
      stretch) release_version=9;;
      buster) release_version=10;;
      bullseye) release_version=11;;
      bookworm) release_version=12;;
      focal) release_version=2004;;
      # jammy unsupported: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=892664
      # jammy) release_version=2204;;
      *) echo "Can't recognize version from $release" >&2; exit 1;;
    esac
}

install_prerequisite_packages() {
  # Set IPv4 precedence over IPv6 to speed up package installation
  if ! grep -q "^precedence ::ffff:0:0/96  100" /etc/gai.conf ; then
     echo "precedence ::ffff:0:0/96  100" | APPLY tee -a /etc/gai.conf >/dev/null
  fi

  apt install -y efibootmgr zstd pwgen
}

get_partition_delimiter() {
    case "$1" in
      *[0-9]) echo "p" ;;
    esac
}

# https://askubuntu.com/questions/354915/quote-command-in-the-shell
shellquote() {
    echo `for item in "$@"; do
        echo "$item" | sed \
            -e "s/'/'\\\\\''/g" -e "s/^.*$/'\\0'/" \
            -e "s/^'\\([a-z0-9/_.\\-]\\+\\)'$/\\1/i" \
            -e "s/%/%%/g"
    done`
}

netmask_from_address() {
    if [ $# -eq 0 ]; then
        read line
        set -- $line
    fi
    case "$1" in
    */32)   echo 255.255.255.255;;
    */31)   echo 255.255.255.254;;
    */30)   echo 255.255.255.252;;
    */29)   echo 255.255.255.248;;
    */28)   echo 255.255.255.240;;
    */27)   echo 255.255.255.224;;
    */26)   echo 255.255.255.192;;
    */25)   echo 255.255.255.128;;
    */24)   echo 255.255.255.0;;
    */23)   echo 255.255.254.0;;
    */22)   echo 255.255.252.0;;
    */21)   echo 255.255.248.0;;
    */20)   echo 255.255.240.0;;
    */19)   echo 255.255.224.0;;
    */18)   echo 255.255.192.0;;
    */17)   echo 255.255.128.0;;
    */16)   echo 255.255.0.0;;
    */15)   echo 255.254.0.0;;
    */14)   echo 255.252.0.0;;
    */13)   echo 255.248.0.0;;
    */12)   echo 255.240.0.0;;
    */11)   echo 255.224.0.0;;
    */10)   echo 255.192.0.0;;
    */9)    echo 255.128.0.0;;
    */8)    echo 255.0.0.0;;
    */7)    echo 254.0.0.0;;
    */6)    echo 252.0.0.0;;
    */5)    echo 248.0.0.0;;
    */4)    echo 240.0.0.0;;
    */3)    echo 224.0.0.0;;
    */2)    echo 192.0.0.0;;
    */1)    echo 128.0.0.0;;
    */0)    echo 0.0.0.0;;
    esac
}

get_network_settings() {

#main_network_device=`sed -n 's/^\([^\t]\+\)\t0\+\t.*/\1/p' /proc/net/route`

route_to_the_outside=`ip -4 route get 8.8.8.8`
if [ -z "$route_to_the_outside" ]; then
    echo "Could not detect main network device from ip route output"
    ip -4 route
    exit 1
fi
main_network_device=`echo "$route_to_the_outside" | grep -Po '(?<=dev )(\S+)'`
if [ -z "$main_network_device" ]; then
    echo "Could not detect main network device from ip route output:"
    echo "$route_to_the_outside"
    exit 1
fi
echo "Using main network device: $main_network_device"
main_routing_table=`ip route show dev $main_network_device`
if [ -z "$main_routing_table" ]; then
    echo "Could not detect main routing table from ip route output on $main_network_device"
    ip -4 route
    exit 1
fi
ipv4_address=`ip -4 addr show dev $main_network_device | grep -Po 'inet \K[\d.]+'`
if [ -z "$ipv4_address" ]; then
    echo "Could not detect main network device IPv4 address from ip addr output:"
    ip -4 addr show
    exit 1
fi
echo "Using IPv4 address: $ipv4_address"
ipv4_gateway=`echo "$main_routing_table" | grep -Po 'via \K[\d.]+' | head -1`
if [ -z "$ipv4_gateway" ]; then
    echo "Could not detect main network device IPv4 gateway from ip route output:"
    echo "$main_routing_table"
    exit 1
fi
echo "Using IPv4 gateway: $ipv4_gateway"
ipv4_first_entry=`echo "$main_routing_table" | grep / | head -1`
if [ -z "$ipv4_first_entry" ]; then
    ptp_route_to_default_gw=`echo "$main_routing_table" | grep ^"$ipv4_gateway "`
    if [ -n "$ptp_route_to_default_gw" ]; then
        echo "Point-to-point route to default gateway found"
        ipv4_netmask=`echo $ipv4_address/32 | netmask_from_address`
    else
        echo "Could not detect main network device IPv4 netmask from ip route output:"
        echo "$main_routing_table"
        exit 1
    fi
else
    ipv4_netmask=`echo "$ipv4_first_entry" | netmask_from_address`
    ipv4_cidr_netmask=`echo "$ipv4_first_entry" | sed -e 's/.*\/\([[:digit:]]\+\) .*/\1/'`
    if [ -z "$ipv4_netmask" ]; then
        echo "Could not parse main network device IPv4 netmask from ip route output:"
        echo "$main_routing_table"
        exit 1
    fi
fi
echo "Using IPv4 netmask: $ipv4_netmask"

#ipv6_address=`dig +short -x $ipv4_address | xargs dig +short AAAA`

ipv6_global_info=`ip -6 addr show dev $main_network_device scope global`
if [ -z "$ipv6_global_info" ]; then
    echo "Could not detect main network device IPv6 info from ip addr output, skipping..."
    # Fall through, not a fatal error
else
    ipv6_address_netmask=`echo "$ipv6_global_info" | grep global | awk '{print $2}'`
    if [ -z "$ipv6_address_netmask" ]; then
        echo "Could not detect main network device IPv6 address and netmask from ip addr output:"
        echo "$ipv6_global_info"
        exit 1
    fi
    ipv6_address=`echo "$ipv6_address_netmask" | cut -d "/" -f 1`
    if [ -z "$ipv6_address" ]; then
        echo "Could not detect main network device IPv6 address from ip addr output:"
        echo "$ipv6_global_info"
        exit 1
    fi
    echo "Using IPv6 address: $ipv6_address"
    ipv6_netmask=`echo "$ipv6_address_netmask" | cut -d "/" -f 2`
    if [ -z "$ipv6_netmask" ]; then
        echo "Could not detect main network device IPv6 netmask from ip addr output:"
        echo "$ipv6_global_info"
        exit 1
    fi
    echo "Using IPv6 netmask: $ipv6_netmask"
    ipv6_default_routes=`ip -6 route show dev $main_network_device default`
    if [ -z "$ipv6_default_routes" ]; then
        echo "Could not detect main network device IPv6 default routes from ip route output"
        ip -6 route
        exit 1
    fi
    ipv6_gateway=`echo "$ipv6_default_routes" | awk '{print $3}' | head -1`
    if [ -z "$ipv6_gateway" ]; then
        echo "Could not detect main network device IPv6 gateway from ip route output:"
        echo "$ipv6_default_routes"
        exit 1
    fi
    echo "Using IPv6 gateway: $ipv6_gateway"
fi

} # get_network_settings

write_usage_information() {

    cat <<EOF
Usage:  $basename [-bDEhPps] [-r stable]
                  [-B <earliest phase>]
                  [-I <identities-file>]
                  [-R <remote>]
                  [-H <hostname>]
                  [-S <disk size>]
                  [-i package[,package,...]]
                  [-m mirror-url]
                  [--] <target device list>

Initiates the bootstrap of a physical machine.

The target device list is a space-delimited list of block devices
upon which to bootstrap the system.

Options:
  -b        apply all phases of bootstrap, including the earliest ones
            that overwrite all data
  -B PHASE  apply only the PHASE of bootstrap and afterwards, allowing
            one to skip certain phases:
              1) basic raw disk setup (overwrites all data)
              2) RAID setup (overwrites all data)
              3) LVM setup (overwrites all data)
              4) filesystem setup (overwrites all data)
              5) debootstrap - base system installation
              6) configuration of the installed system
              7) hardware-related packages installation
              8) enabling of SSH
  -d DIST   Specify distribution to be installed: debian (default) or ubuntu
  -D        use a single block device intentionally
            This is typically used with hardware RAID arrays
            Otherwise we set up a mdadm software RAID array even if degraded
  -E        use an EFI boot loader
  -h        show this help
  -H HOST   use HOST as the final host name
  -i PKGS   pre-seed a comma-delimited list of PKGS in debootstrap
  -I PUBKEY Identity/public-key file to copy into ~/.ssh/authorized_keys on
            the remote machine
            Has to be specified prior to -R, when used
            Applies the default behaviour of ssh-copy-id, when unspecified
  -m URL    use URL as the Debian mirror in debootstrap
  -a        Auto detect/configure networking device in GRUB config
  -n        don't set up network as a bonding device and a bridge
            This is typically used when virtual machine usage is not desired
  -P        use a point-to-point setup for IPv4
  -p        use a point-to-point setup for IPv6
  -r NAME   use NAME as the Debian release in debootstrap
  -k        Fetch repository key for specified release from Debian archive
            This is necessary if the release executing this diverges from
            the release to be installed
  -R REMOTE use REMOTE as the specification of remote SSH user@host
            where the contents of this script is copied in its
            entirety and then run
            Note that this option must be given as the first one, otherwise the
            preceding options will not be applied on the REMOTE host
  -s        use first serial port (ttyS0) for kernel serial console
  -S SIZE   use SIZE as the size of the root disk logical volume

Examples:
        $basename -R node-1.example.com -- /dev/sda /dev/sdb
        $basename -R root@ovh-IP -H final-host.name.domain.tld -i vim,joe,less,man-db -E -p -s -b /dev/nvme0n1 /dev/nvme1n1
        $basename -R root@hetzner-IP -H final-host.name.domain.tld -i vim,joe,less,man-db -P -b /dev/sda /dev/sdb

        $basename -R root@ovh-IP -H final-host.name.domain.tld -i vim,joe,less,man-db -E -p -s -m http://archive.ubuntu.com/ubuntu/ -d ubuntu -r focal -b /dev/nvme0n1 /dev/nvme1n1
        $basename -R root@hetzner-IP -H final-host.name.domain.tld -i vim,joe,less,man-db -P -m http://archive.ubuntu.com/ubuntu/ -d ubuntu -r focal -b /dev/sda /dev/sdb

EOF

}

# https://en.wikipedia.org/wiki/ANSI_escape_code#Colors
color_green="\033[0;32m"
color_reset="\033[0m"

# Echo commands before execution:
APPLY(){
    sync
    printf "$color_green\$ `shellquote "$@"` $color_reset\n"
    LANG=C "$@"
}

# Schedule cleanup commands executed when the script ends:
STACK="trap - EXIT"
DEFER() {
    STACK="APPLY $*; $STACK"
}

# Cause the shell to call function ABORT when any of the specified signals are
# received. Note the explicit `set +e`: It is supposed to ensure failed cleanup
# commands to not cause processing of the stack to abort.
trap 'set +e; eval "$STACK"' EXIT HUP INT QUIT PIPE TERM

while POSIXLY_CORRECT=1 getopts "I:H:R:bB:hi:m:r:aEPpsS:d:nDk" OPTION; do
    case "$OPTION" in

    I)  identities_file="-i $OPTARG"
        ;;

    R)  set -- _ "$@"
        shift $OPTIND
        ssh -oBatchMode=yes "$OPTARG" exit || ssh-copy-id $identities_file $OPTARG
        echo "Executing this script at $OPTARG..."
        exec ssh "$OPTARG" -- /bin/sh /dev/stdin "$@" <"$0"
        ;;

    d)  distribution="$OPTARG"
        ;;

    D)  want_single_disk_device=1
        ;;

    H)  hostname="$OPTARG"
        ;;

    b)  min_bootstrap_phase=1
        ;;

    B)  min_bootstrap_phase="$OPTARG"
        case "$min_bootstrap_phase" in
          [0-9]) : # it's fine
                ;;
          *) echo "Value for option -B must be an integer!" >&2
             exit 1
                ;;
        esac
        ;;

    E)  extra_efi_rules=1
        efi_optional="false"
        bios_boot_partition_type="ef00"
        packages_grub="grub-efi"
        packages_efi="efibootmgr"
        ;;

    P)  ipv4_network_point_to_point=1
        ;;

    p)  ipv6_network_point_to_point=1
        ;;

    n)  network_bonding_bridge=
        ;;

    a)  auto_nic_grub=1
        ;;

    s)  console_options="$console_options console=ttyS0,115200n8"
        ;;

    h)  write_usage_information
        exit 0
        ;;

    S)  root_disk_size="$OPTARG"
        ;;

    i)  packages_input="$OPTARG"
        ;;

    m)  mirror="$OPTARG"
        ;;

    r)  release="$OPTARG"
        ;;

    k)  extra_release_key=1
        ;;

    \?) write_usage_information >&2
        exit 2
        ;;

    esac
done

# Discard all arguments processed already:
set -- _ "$@"
shift $OPTIND

# Explicate what part of the script we're going to do
echo "Running bootstrap phases from $min_bootstrap_phase onwards..."

# Map release version from release name
map_release_version

# Map release version from release name
install_prerequisite_packages

# Determine default network settings.
get_network_settings

# Hetzner has a special pointopoint routing setup for IPv4, see
# https://wiki.hetzner.de/index.php/Netzkonfiguration_Debian
if [ ! -z "$ipv4_network_point_to_point" ]; then
    ipv4_netmask="255.255.255.255"
    ipv4_cidr_netmask="32"
fi

# OVH has a special pointtopoint routing setup for IPv6, see
# https://docs.ovh.com/gb/en/dedicated/network-ipv6/
if [ ! -z "$ipv6_network_point_to_point" ]; then
    ipv6_netmask="128"
fi

# We could technically do this prior to the first apt install command,
# but it happens to get done in two code paths that way, so just do
# a Debian Live detection instead
if test -d /lib/live/mount/medium && mountpoint /lib/live/mount/medium; then
    # Generate the necessary locales to make sure our current environment
    # doesn't emit numerous spurious locale warnings
    command -v locale-gen || APPLY apt install --no-install-recommends locales
    locale_gen_config=/etc/locale.gen
    environment_locales=$(locale | cut -d= -f2 | sort -u | grep -v ^C$ | tr -d '"' | sort -u | xargs)
    echo $environment_locales
    for locale in $environment_locales; do
        short_locale=${locale#*.}
        if [ "$locale" != "$short_locale" ]; then
            full_locale="$locale $short_locale"
        else # otherwise just do an educated guess and hope for the best
            full_locale="$short_locale ISO-8859-1"
        fi
        APPLY sed -i "/^# $full_locale/s/^# //" $locale_gen_config
        if ! APPLY grep -q "^$full_locale" $locale_gen_config; then
            echo "$full_locale" | APPLY tee -a $locale_gen_config >/dev/null
        fi
    done
    APPLY locale-gen
fi

# Try to apply mdadm more often, except in legitimately redundant cases
# such as having a single hardware RAID device already
if [ -z "$want_single_disk_device" ]; then
    unified_crypt_device=/dev/md/lvm
    if [ "$min_bootstrap_phase" -gt 1 ]; then
        if [ ! -e "$unified_crypt_device" ]; then
            if [ -n "$hostname" -a -b "/dev/md/$hostname:lvm" ]; then
                APPLY ln -s "/dev/md/$hostname:lvm" "$unified_crypt_device"
            else
                echo "Failed to open unified crypt device inside /dev/md:"
                APPLY ls -l /dev/md
                APPLY ls -l "$unified_crypt_device" # fail
            fi
        fi
    fi
    unified_boot_device=/dev/md/boot
    if [ "$min_bootstrap_phase" -gt 1 ]; then
        if [ ! -e "$unified_boot_device" ]; then
            if [ -n "$hostname" -a -b "/dev/md/$hostname:boot" ]; then
                APPLY ln -s "/dev/md/$hostname:boot" "$unified_boot_device"
            else
                echo "Failed to open unified boot device inside /dev/md:"
                APPLY ls -l /dev/md
                APPLY ls -l "$unified_boot_device" # fail
            fi
        fi
    fi
    command -v mdadm || APPLY env DEBIAN_FRONTEND=noninteractive apt install --no-install-recommends mdadm
    pidof mdadm && APPLY pkill mdadm || true
    packages_md="mdadm"
else
    device=$1
    partition_delimiter=$(get_partition_delimiter $device)
    unified_crypt_device="${device}${partition_delimiter}3"
    unified_boot_device="${device}${partition_delimiter}2"
fi

if [ ! -z "$hostname" ]; then
    APPLY hostname -b "$hostname"
fi

command -v cryptsetup || APPLY apt install cryptsetup-bin
packages_crypto="cryptsetup cryptsetup-initramfs dropbear dropbear-initramfs"

if [ "$min_bootstrap_phase" -le 1 ]; then

    APPLY lsblk
    APPLY efibootmgr || $efi_optional

    # Discard existing software RAID arrays, if any:
    # Maybe we should also do this in case of a single disk device, possibly
    # in the case of fake RAID devices, but hopefully we never see that on
    # actual servers
    if [ "$min_bootstrap_phase" -le 2 -a -z "$want_single_disk_device" ]; then
        for premddev in /dev/md/* /dev/md?*; do
            test -e $premddev || continue # if we already processed it under an alias
            APPLY ls -la $premddev || continue # if the race condition still hits
            APPLY mdadm --stop $premddev
        done
        APPLY cat /proc/mdstat
    fi

    # Used to identify and reference the target devices that make up the BOOT
    # and LVM partitions within the software RAID setup (see section below):
    custom_boot_devices=
    custom_crypt_devices=

    for device in "$@"; do

        partition_delimiter=$(get_partition_delimiter $device)

        custom_boot_devices="${custom_boot_devices} ${device}${partition_delimiter}2"
        custom_crypt_devices="${custom_crypt_devices} ${device}${partition_delimiter}3"

        command -v sgdisk || APPLY apt install gdisk
        APPLY sgdisk --clear --mbrtogpt \
                     -n 0:0:+512M -c 0:EFI  -t 0:$bios_boot_partition_type \
                     -n 0:0:+512M -c 0:BOOT -t 0:fd00 \
                     -n 0:0:0     -c 0:LVM  -t 0:fd00 \
                     -p "$device"

        command -v mkfs.vfat || APPLY apt install dosfstools
        APPLY mkfs.vfat -F 32 "${device}${partition_delimiter}1"
        # NB: we don't try to create a RAID1 across the EFI boot partitions
        # because of possible problems, cf.
        # https://outflux.net/blog/archives/2018/04/19/uefi-booting-and-raid1/

    done

    if [ -z "$want_single_disk_device" -a $# = 1 ]; then
        # "missing" is a special keyword that mdadm will recognize later
        custom_boot_devices="$custom_boot_devices missing"
        custom_crypt_devices="$custom_crypt_devices missing"
    fi

    if [ "$min_bootstrap_phase" -le 2 -a -z "$want_single_disk_device" ]; then

        default_dev_raid_speed_limit_max=$(sysctl -n dev/raid/speed_limit_max)
        APPLY sysctl -w dev/raid/speed_limit_max=1000
        DEFER sysctl -w dev/raid/speed_limit_max=$default_dev_raid_speed_limit_max

        for device in $custom_boot_devices; do
            if [ "$device" = "missing" ]; then
                continue
            fi
            APPLY mdadm --zero-superblock $device
        done

        raid_device_count=$#
        if [ $# = 1 ]; then
            raid_device_count=2
        fi

        APPLY mdadm --create "$unified_boot_device" \
                    --level=raid1 \
                    --raid-devices=$raid_device_count \
                    --run \
                    $custom_boot_devices

        for device in $custom_crypt_devices; do
            if [ "$device" = "missing" ]; then
                continue
            fi
            APPLY mdadm --zero-superblock $device
        done

        APPLY mdadm --create "$unified_crypt_device" \
                    --level=raid10 \
                    --raid-devices=$raid_device_count \
                    --run \
                    $custom_crypt_devices
    fi

    echo "changeme" |
        APPLY cryptsetup \
                         --cipher aes-xts-plain64 \
                         --key-size 512 \
                         --iter-time 10000 \
                         --hash sha512 \
                         luksFormat "$unified_crypt_device"
fi

# Determine the UUID of the LVM partition:
APPLY blkid -o value "$unified_crypt_device"
uuid=`blkid -o value "$unified_crypt_device" | head -1`
test -n "$uuid" # make sure it's not empty

if [ -b /dev/mapper/crypto-lvm ]; then
    unified_crypt_device_dm_info=$(dmsetup info \
                                           --columns --noheadings -o open,attr \
                                           /dev/mapper/crypto-lvm)
fi
if [ "$unified_crypt_device_dm_info" = "1:L--w" ]; then
    # It's already open and live, just show the status
    APPLY dmsetup info /dev/mapper/crypto-lvm
else
    echo "changeme" |
        APPLY cryptsetup luksOpen "$unified_crypt_device" crypto-lvm
fi

DEFER cryptsetup luksClose crypto-lvm

command -v pvcreate || APPLY apt install --no-install-recommends -y lvm2
packages_lvm="lvm2"

if [ "$min_bootstrap_phase" -le 3 ]; then

    APPLY pvcreate /dev/mapper/crypto-lvm

    APPLY vgcreate vg0 /dev/mapper/crypto-lvm

fi

APPLY vgchange --activate ey vg0
DEFER vgchange --activate n vg0

if [ "$min_bootstrap_phase" -le 4 ]; then

    # Create the logical volume for the initial root file system:
    APPLY lvcreate --size $root_disk_size --name lv0 vg0

    # Create root (/) file system:
    APPLY mkfs.ext4 /dev/mapper/vg0-lv0

fi

# Mount root (/) file system:
if ! mountpoint -q /mnt; then
    APPLY mount /dev/mapper/vg0-lv0 /mnt
fi
DEFER umount /mnt

if [ "$min_bootstrap_phase" -le 4 ]; then

    # Create boot (/boot) file system:
    APPLY mkfs.ext2 "$unified_boot_device"

fi

if [ ! -d /mnt/boot ]; then
    APPLY mkdir /mnt/boot
fi

# Mount boot (/boot) file system:
if ! mountpoint -q /mnt/boot; then
    APPLY mount "$unified_boot_device" /mnt/boot
fi
DEFER umount /mnt/boot

if [ "$min_bootstrap_phase" -le 5 ]; then

    if [ "$extra_release_key" = "1" ]; then
      command -v wget || APPLY apt install -y --no-install-recommends wget
      command -v gpg || APPLY apt install -y --no-install-recommends gpg
      wget https://ftp-master.debian.org/keys/release-$release_version.asc -qO- | \
      gpg --import \
          --no-default-keyring \
          --keyring /usr/share/keyrings/debian-archive-keyring.gpg
    fi

    if [ "$distribution" = "ubuntu" ]; then
      APPLY apt install -y --no-install-recommends ubuntu-archive-keyring || \
          echo "Attempting bootstrap without 'ubuntu-archive-keyring'."
    fi

    packages="busybox"
    packages="$packages,console-setup,kbd"
    packages="$packages,openssh-server"
    packages="$packages,sudo"
    packages="$packages,locales"

    if [ "$release_version" -gt 10 ]; then
        packages="$packages,python3"
    else
        packages="$packages,python"
    fi

    if [ "$network_bonding_bridge" = "1" ]; then
        packages="$packages,ifenslave"
        packages="$packages,bridge-utils"
    fi

    if [ -n "$packages_input" ]; then
        packages="$packages,$packages_input"
    fi

    components="main,non-free,contrib"
    if [ "$distribution" = "ubuntu" ]; then
        components="main,restricted,universe"
    fi
    if [ "$release" = "bookworm" ]; then
        components="main,non-free,non-free-firmware,contrib"
    fi

    command -v debootstrap || APPLY apt install -y debootstrap
    if [ -d /mnt/debootstrap/ ]; then
        APPLY echo "Warning: detected previous debootstrap attempt..."
        rm -r /mnt/debootstrap/
    fi

    # debootstrap links all the releases for debian to 'sid' and ubuntu to 'gutsy' by default.
    # Link chroot-script for release to respective source script, if it happens to not exist.
    if [ ! -f /usr/share/debootstrap/scripts/$release ]; then

        debootstrap_script_source_version=""
        if [ "$distribution" = "debian" ]; then
            debootstrap_script_source_version="sid"
        elif [ "$distribution" = "ubuntu" ]; then
            debootstrap_script_source_version="gutsy"
        fi

        ln -s /usr/share/debootstrap/scripts/$debootstrap_script_source_version \
          /usr/share/debootstrap/scripts/$release

    fi

    if ! APPLY debootstrap \
                           --components $components \
                           --include="$packages" \
                           "$release" \
                           /mnt \
                           "$mirror"; then
        APPLY tail -50 /mnt/debootstrap/debootstrap.log
        false # make sure we still fail
    fi
fi

if [ "$min_bootstrap_phase" -le 6 ]; then

    if [ "$release_version" -gt 10 ]; then
        release_security="$release-security"
    else
        release_security="$release"
    fi

    # Circumvent random MAC overwrite on bond device and
    # all interfaces that are related to it.
    # https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=949062
    if [ "$release_version" -gt 10 ]; then
      APPLY tee /mnt/etc/systemd/network/99-default.link <<END
[Match]
OriginalName=bond*

[Link]
MACAddressPolicy=none
END
    fi

    if [ "$distribution" = "debian" ]; then
      if [ "$release" = "bookworm" ]; then
        apt_repositories="deb $mirror $release main non-free non-free-firmware contrib
deb-src $mirror $release main non-free non-free-firmware contrib
deb http://security.debian.org/debian-security $release_security/updates main non-free non-free-firmware contrib
deb-src http://security.debian.org/debian-security $release_security/updates main non-free non-free-firmware contrib
"
      else
        apt_repositories="deb $mirror $release main non-free contrib
deb-src $mirror $release main non-free contrib
deb http://security.debian.org/debian-security $release_security/updates main non-free contrib
deb-src http://security.debian.org/debian-security $release_security/updates main non-free contrib
"
      fi
    elif [ "$distribution" = "ubuntu" ]; then
        apt_repositories="deb $mirror $release main restricted
deb-src $mirror $release main restricted
deb $mirror $release universe
deb-src $mirror $release universe

deb http://security.ubuntu.com/ubuntu $release-security main restricted
deb-src http://security.ubuntu.com/ubuntu $release-security main restricted
deb http://security.ubuntu.com/ubuntu $release-security universe
deb-src http://security.ubuntu.com/ubuntu $release-security universe
"
    fi

    APPLY tee /mnt/etc/apt/sources.list <<END
$apt_repositories
END

    APPLY tee /mnt/etc/fstab <<END
# <file system> <mount point> <type> <options> <dump> <pass>
/dev/mapper/vg0-lv0 / ext4 defaults 0 1
$unified_boot_device /boot ext2 defaults,noatime 0 2
END

    APPLY tee /mnt/etc/crypttab <<END
# <target name> <source device> <key file> <options>
crypto-lvm UUID=$uuid none luks
END

    # do this cheap runtime check, in case it changes
    case "$network_device_options" in
      *net.ifnames=0*)
        # we could be booted into a kernel that doesn't use new-style names
        case "$main_network_device" in
            eth*)
            APPLY echo "Leaving main network device $main_network_device in the booted system"
            ;;
            *)
            APPLY echo "Setting main network device from $main_network_device to eth0 in the booted system"
            main_network_device="eth0"
            ;;
        esac
      ;;
    esac

    if [ "$network_bonding_bridge" = "1" ]; then
        gateway_device=br0
    else
        gateway_device=$main_network_device
    fi

    if [ "$distribution" = "debian" ]; then
    # for the pre-up line below, see https://blog.tausys.de/2014/06/02/initramfs-dropbear-und-ipv6/
    # (the kernel IP config done because of dropbear would cause userland ifup
    # to fail to add duplicate settings)

        ipv4_gateway_configuration="gateway $ipv4_gateway"
        if [ ! -z "$ipv4_network_point_to_point" ]; then
           ipv4_gateway_configuration="$ipv4_gateway_configuration
    pointopoint $ipv4_gateway
"
        fi

        ipv6_gateway_configuration="gateway $ipv6_gateway"
        if [ ! -z "$ipv6_network_point_to_point" ]; then
           ipv6_gateway_configuration="
    post-up ip -f inet6 route add $ipv6_gateway dev $gateway_device
    post-up ip -f inet6 route add default via $ipv6_gateway dev $gateway_device
    pre-down ip -f inet6 route del $ipv6_gateway dev $gateway_device
    pre-down ip -f inet6 route del default via $ipv6_gateway dev $gateway_device
"
        fi

        if [ "$network_bonding_bridge" = "1" ]; then
            interface_file=/mnt/etc/network/interfaces.d/$gateway_device
            interface_ipv4_prologue="
auto bond0
iface bond0 inet manual
  slaves $main_network_device
  bond_mode active-backup
  bond_miimon 100

auto br0
iface br0 inet static
  bridge-ports bond0
  bridge-fd 0"
        interface_ipv6_prologue="
iface br0 inet6 static"
        else
            interface_file=/mnt/etc/network/interfaces.d/$main_network_device
            interface_ipv4_prologue="
auto  $main_network_device
iface $main_network_device inet static"
        interface_ipv6_prologue="
iface $main_network_device inet6 static"
        fi
        APPLY cp /dev/null $interface_file

        APPLY tee -a $interface_file <<END
$interface_ipv4_prologue
    address   $ipv4_address
    netmask   $ipv4_netmask
    pre-up    ip addr flush dev $main_network_device
    $ipv4_gateway_configuration
END

        if [ ! -z "$ipv6_address" ]; then
            APPLY tee -a $interface_file <<END

$interface_ipv6_prologue
    address $ipv6_address
    netmask $ipv6_netmask
    dad-attempts 150
    $ipv6_gateway_configuration
END
        else
            APPLY tee -a $interface_file <<END

# FIXME add IPv6!
#$interface_ipv6_prologue
#    address $ipv6_address
#    netmask $ipv6_netmask
#    dad-attempts 150
#    $ipv6_gateway_configuration
END
        fi

    elif [ "$distribution" = "ubuntu" ]; then
        interface_file=/mnt/etc/netplan/config.yaml
        interface_netplan_prologue="network:
  version: 2
  renderer: networkd"
        interface_ipv4_netplan_address="- $ipv4_address/$ipv4_cidr_netmask"
        interface_ipv6_netplan_address="- $ipv6_address/$ipv6_netmask"

        interface_netplan_addresses="addresses:
        $interface_ipv4_netplan_address
        $interface_ipv6_netplan_address"

        interface_ipv4_netplan_route="gateway4: $ipv4_gateway"
        if [ ! -z "$ipv4_network_point_to_point" ]; then
           interface_ipv4_netplan_route="routes:
        - to: \"$ipv4_gateway\"
          scope: link
        - to: 0.0.0.0/0
          via: \"$ipv4_gateway\"
          on-link: true
"
        fi

        interface_ipv6_netplan_route="gateway6: $ipv6_gateway
      accept-ra: true"

        if [ ! -z "$ipv6_network_point_to_point" ]; then
           interface_ipv6_netplan_route="routes:
        - to: \"$ipv6_gateway\"
          scope: link
        - to: 0::0/0
          via: \"$ipv6_gateway\"
          on-link: true
"
        fi

        if [ "$network_bonding_bridge" = "1" ]; then
            interface_mac_address=`ip link show eth0 | grep -o -E  \
                "link/ether ..:..:..:..:..:.." | awk '{print $2}'`

            APPLY tee -a $interface_file <<END
$interface_netplan_prologue
  ethernets:
    $main_network_device:
      dhcp4: false
      dhcp6: false
  bonds:
    bond0:
      dhcp4: false
      dhcp6: false
      interfaces:
        - $main_network_device
  bridges:
    br0:
      interfaces:
        - bond0
      macaddress: $interface_mac_address
      dhcp4: false
      dhcp6: false
      $interface_netplan_addresses
      $interface_ipv4_netplan_route
      $interface_ipv6_netplan_route
END
        else
            APPLY tee -a $interface_file <<END
$interface_netplan_prologue
  ethernets:
    $main_network_device:
      $interface_netplan_addresses
      $interface_ipv4_netplan_route
      $interface_ipv6_netplan_route
END
        fi

        resolv_conf_source="/etc/resolv.conf"
        if [ -L /etc/resolv.conf ]; then
            resolv_conf_source="/run/systemd/resolve/resolv.conf"
        fi

        APPLY rm /mnt/etc/resolv.conf
        grep ^nameserver $resolv_conf_source | \
            APPLY tee /mnt/etc/resolv.conf

    fi

# disable IPv6 autoconf and router advertising to prevent known issues
# see https://docs.ovh.com/gb/en/dedicated/network-ipv6/
    APPLY tee /mnt/etc/sysctl.d/local-static-ipv6.conf <<END
net.ipv6.conf.all.autoconf=0
net.ipv6.conf.all.accept_ra=0
net.ipv6.conf.default.autoconf=0
net.ipv6.conf.default.accept_ra=0
END

    # https://www.kernel.org/doc/Documentation/filesystems/nfs/nfsroot.txt
    early_ip_options="ip=$ipv4_address::$ipv4_gateway:$ipv4_netmask::$main_network_device:off"

    if [ "$auto_nic_grub" = "1" ]; then
      early_ip_options="ip=::::::dhcp"
    fi

    if [ ! -z "$hostname" ]; then
        echo "$hostname" | APPLY tee /mnt/etc/hostname
    fi

    # Generate typical locales
    locale_gen_config=/mnt/etc/locale.gen
    locale_status=$(md5sum $locale_gen_config)
    for locale in "en_US.UTF-8"; do
        full_locale="$locale ${locale#*.}"
        APPLY sed -i "/^# $full_locale/s/^# //" $locale_gen_config
        if ! APPLY grep -q "^$full_locale" $locale_gen_config; then
            echo "$full_locale" | APPLY tee -a $locale_gen_config >/dev/null
        fi
    done
    if [ "$locale_status" != "$(md5sum $locale_gen_config)" ]; then
        APPLY chroot /mnt locale-gen
    fi

fi

if [ "$min_bootstrap_phase" -le 7 ]; then

    # Bind the device index for use within chroot:
    APPLY mount --bind /dev /mnt/dev
    DEFER umount /mnt/dev

    # Bind the process FS for use within chroot:
    APPLY mount -t proc none /mnt/proc
    DEFER umount /mnt/proc

    # Bind the system FS for use within chroot:
    APPLY mount --bind /sys /mnt/sys
    DEFER umount /mnt/sys

    # Bind the run FS for use within chroot:
    APPLY mount --bind /run /mnt/run
    DEFER umount /mnt/run

    # Bind a temporary FS for use within chroot:
    APPLY mount -t tmpfs none /mnt/tmp
    DEFER umount /mnt/tmp

    # The kernel and boot loader packages can trigger grub-install
    # and update-grub operations themselves, and fail, so delay their
    # installation to after we have the device mappings set up
    # within the chroot

    # A possible alternative is to try to prepare devices ourselves:
    # if [ ! -b /mnt/dev/mapper/vg0-lv0 ]; then
    #     APPLY mkdir -p /mnt/dev/mapper
    #     real_block_device=$(readlink -f /dev/mapper/vg0-lv0)
    #     APPLY cp -av $real_block_device /mnt/dev/mapper/vg0-lv0
    # fi
    # cf.
    # https://salsa.debian.org/dsa-team/mirror/dsa-misc/-/blob/master/scripts/VM-installs/ganeti-unified

    command -v dpkg >/dev/null || sudo yum install dpkg # :)
    architecture="`dpkg --print-architecture`"
    packages_kernel=""

    if [ "$distribution" = "debian" ]; then
        packages_kernel="linux-image-$architecture"
        packages_kernel="$packages_kernel firmware-bnx2x"
        packages_kernel="$packages_kernel firmware-linux-free"
        packages_kernel="$packages_kernel firmware-realtek"

    elif [ "$distribution" = "ubuntu" ]; then

        packages_kernel="linux-image-generic"
        packages_kernel="$packages_kernel linux-generic"
        packages_kernel="$packages_kernel linux-headers-generic"
        packages_kernel="$packages_kernel linux-firmware"
        packages_kernel="$packages_kernel initramfs-tools"
    fi

    packages_kernel="$packages_kernel intel-microcode"
    packages_kernel="$packages_kernel amd64-microcode"

    APPLY chroot /mnt \
             env DEBIAN_FRONTEND=noninteractive \
             apt install -y --no-install-recommends \
                 zstd
    APPLY chroot /mnt \
             env DEBIAN_FRONTEND=noninteractive \
             apt install -y --no-install-recommends \
                 $packages_kernel
    APPLY chroot /mnt \
             env DEBIAN_FRONTEND=noninteractive \
             apt install -y --no-install-recommends \
                 $packages_md $packages_lvm $packages_crypto

    # NB: this assumes lvm2 was part of $packages_later
    # There shall only be one LVM daemon, and since we're trying to take care
    # of that stuff outside the actual system we need to disable its own one:
    if APPLY grep use_lvmetad /mnt/etc/lvm/lvm.conf; then
    APPLY sed -i 's/^\(\s*use_lvmetad\s*=\).*/\1 0/' /mnt/etc/lvm/lvm.conf
    fi
    for service in lvm2-lvmetad.service lvm2-lvmetad.socket; do
        APPLY chroot /mnt systemctl is-enabled $service 2>/dev/null || continue
        APPLY chroot /mnt systemctl disable $service
    done

    if [ -z "$packages_grub" ]; then
        packages_grub="grub-pc"
    fi

    APPLY chroot /mnt \
             env DEBIAN_FRONTEND=noninteractive \
             apt install -y --no-install-recommends \
                 $packages_grub $packages_efi

    if [ "$distribution" = "ubuntu" ]; then
        APPLY sed -i -e 's/quiet splash\"/quiet text nomodeset\"/' /mnt/etc/default/grub
    fi

    if ! APPLY grep -q "^# added by system-bootstrap:" /mnt/etc/default/grub; then
        APPLY tee -a /mnt/etc/default/grub <<END
# added by system-bootstrap:
GRUB_CMDLINE_LINUX="$console_options $network_device_options $early_ip_options"
GRUB_ENABLE_CRYPTODISK=y
END

    # maybe make grub console setup dependent on $console_options too?
        APPLY tee -a /mnt/etc/default/grub <<END
GRUB_TERMINAL="console serial"
GRUB_SERIAL_COMMAND="serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1"
END
    fi

fi

# this is realistically part of the previous phase, but it shouldn't do
# irreparable changes to the installed system so it runs unconditionally

# Without the efivars filesystem mounted, some machines exhibit
# unreadable EFI variables, which can then cause grub commands to fail
if [ ! -z "$extra_efi_rules" ]; then
    if ! mountpoint -q /sys/firmware/efi/efivars; then
        APPLY mount -t efivarfs efivarfs /sys/firmware/efi/efivars
    fi
    DEFER umount /sys/firmware/efi/efivars
fi

for device in "$@"; do

    # Some UEFI boot loader stuff
    # taken from https://gist.github.com/ppmathis/ccfbfce86484dc61834c1f17568d7b80
    # ...except for the merge of 2x EFI grub-install into update-grub
    # which looks like overkill

    if [ ! -z "$extra_efi_rules" ]; then

        partition_delimiter=$(get_partition_delimiter $device)
        APPLY ls -l ${device}${partition_delimiter}1 # or fail
        # Create mount points for UEFI file systems:
        efi_mountpoint=/boot/grub-efi-$(basename $device)
        if [ ! -d /mnt/$efi_mountpoint ]; then
            APPLY mkdir -p /mnt/$efi_mountpoint
        fi
        APPLY mount -t vfat ${device}${partition_delimiter}1 \
                            /mnt/$efi_mountpoint
        DEFER umount /mnt/$efi_mountpoint
        efi_dir_name="GRUB-EFI-$(basename $device)"
        try_efi_manual=0
        # TODO: maybe try x86_64-efi-signed?
        APPLY chroot /mnt grub-install \
                                       --recheck \
                                       --target x86_64-efi \
                                       --efi-directory=$efi_mountpoint \
                                       --bootloader-id=$efi_dir_name \
                                       $device || try_efi_manual=1
        if [ "$try_efi_manual" = 1 ]; then
            APPLY echo "Normal grub-install failed, will try a more manual EFI method now..."
            APPLY chroot /mnt grub-mkstandalone \
                                                --format x86_64-efi \
                                                --directory /usr/lib/grub/x86_64-efi \
                                                --output $efi_mountpoint/EFI/$efi_dir_name/grubx64.efi \
                                                --themes='' \
                                                /boot/grub/grub.cfg
            APPLY efibootmgr -c \
                             -d $device \
                             -p 1 \
                             -w \
                             -L "GRUB EFI ($device)" \
                             -l /EFI/$efi_dir_name/grubx64.efi
        fi
        APPLY ls -la /mnt/$efi_mountpoint/EFI/*
        APPLY efibootmgr

    else # -z $extra_efi_rules

        APPLY chroot /mnt grub-install "$device"

    fi

done

APPLY chroot /mnt update-grub

if [ "$min_bootstrap_phase" -le 8 ]; then
    # OpenSSH should be the obvious bare minimum:
    APPLY chroot /mnt systemctl enable ssh.service
fi

pw=$(pwgen 32 1)
APPLY chroot /mnt passwd <<END
$pw
$pw
END

APPLY mkdir -m 0700 -p /mnt/root/.ssh

# this part could possibly be moved up to where dropbear is installed,
# because it prints a warning about it
if [ "$release" = "jessie" ]; then
    APPLY cp "$authorized_keys" /mnt/etc/initramfs-tools/root/.ssh/authorized_keys
elif [ "$release" = "bookworm" ]; then
    APPLY cp "$authorized_keys" /mnt/etc/dropbear/initramfs/authorized_keys
else
    APPLY cp "$authorized_keys" /mnt/etc/dropbear-initramfs/authorized_keys
fi

APPLY cp "$authorized_keys" /mnt/root/.ssh/authorized_keys

APPLY chroot /mnt update-initramfs -u

APPLY echo "All done."
